angular.module('app').factory('suggestionsService', ['$timeout','$q', function ($timeout, $q) {
	'use strict';
	return {
		getList: function(keyword){
			var deferred = $q.defer();
			$timeout(function(){
				var randomString = undefined;				
				var data = new Array();
				for(var i = 0; i < 10; i++){
					randomString = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 8);
					data.push({title: keyword + randomString});
				}				
				deferred.resolve(data);				
			}, 100);
			return deferred.promise;
		}
		
	};
	
}]);
