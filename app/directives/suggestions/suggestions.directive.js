angular.module('app').directive('suggestions', [ 'suggestionsService',
	function (suggestionsService) {
		'use strict';

		return {
			restrict: 'E',
			templateUrl: 'app/directives/suggestions/suggestions.html',
			link: function (scope, element, attrs) {
				scope.search = attrs.search;				
				var beforeClick = true;		
				scope.$watch('search', function(newValue,oldValue){
					scope.selectedIndex = -1;
					scope.list = undefined;
					if(newValue !== oldValue &&
						newValue.length >= 3 &&
						beforeClick === true
						){
						
						suggestionsService.getList(newValue).then(function(list){
							scope.list = list;
						});
					}
					else if(beforeClick === false){
						beforeClick = true;
					}
				});
				scope.selectItem = function(item){
					beforeClick = false;
					scope.search = item.title;

				}

				scope.keypress = function(e){
					if(scope.list !== undefined){
						switch(e.which) {					
							case 38: 
								if(scope.selectedIndex > -1){
									scope.selectedIndex--;
								}
							break;
							
							case 40:
								if(scope.selectedIndex < scope.list.length - 1){
									scope.selectedIndex++;
								}
							break;

							case 13:								
								scope.selectItem(scope.list[scope.selectedIndex]);
							break;

							default: return;
						}
					}
				}
			}
			
		};
	}
]);